package com.panneerat.week11;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        Bird bird1 = new Bird("Tweety");
        bird1.eat();
        bird1.sleep();
        bird1.walk();
        bird1.run();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();
        bird1.swim();
        Plane boeing = new Plane("Boeing", "Rosaroi");
        boeing.takeoff();
        boeing.fly();
        boeing.landing();
        Superman clark = new Superman("clark");
        clark.takeoff();
        clark.fly();
        clark.landing();
        clark.walk();
        clark.run();
        clark.eat();
        clark.sleep();
        clark.swim();
        Human mark = new Human("Mark");
        mark.eat();
        mark.sleep();
        mark.walk();
        mark.run();
        mark.swim();
        Fish nemo = new Fish("Nemo");
        nemo.eat();
        nemo.sleep();
        nemo.swim();
        Dog milo = new Dog("Milo");
        milo.eat();
        milo.sleep();
        milo.walk();
        milo.run();
        milo.swim();
        Cat dum = new Cat("Dum");
        dum.eat();
        dum.sleep();
        dum.walk();
        dum.run();
        dum.swim();
        Bat bat1 = new Bat("Max");
        bat1.eat();
        bat1.sleep();
        bat1.takeoff();
        bat1.fly();
        bat1.landing();
        Rat rat1 = new Rat("Ribbon");
        rat1.eat();
        rat1.sleep();
        rat1.walk();
        rat1.run();
        rat1.swim();
        Crocodile cro1 = new Crocodile("Charlotte");
        cro1.eat();
        cro1.sleep();
        cro1.crawl();
        cro1.swim();
        Snake snake1 = new Snake("Micro");
        snake1.eat();
        snake1.sleep();
        snake1.crawl();
        snake1.swim();

        Flyable[] flyables = { bird1, boeing, clark, bat1};
        for (int i = 0; i < flyables.length; i++) {
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }

        Walkable[] walkables = { bird1, clark, mark, dum, milo, rat1 };
        for (int i = 0; i < walkables.length; i++) {
            walkables[i].walk();
            walkables[i].run();
        }

        Swimable[] swimables = { bird1, clark, mark, nemo, milo, dum, rat1, cro1, snake1 };
        for (int i = 0; i < swimables.length; i++) {
            swimables[i].swim();
        }

        Crawlable[] crawlables = { cro1, snake1 };
        for (int i = 0; i < crawlables.length; i++) {
            crawlables[i].crawl();
        }
    }
}
